{ config, pkgs, lib, ... }:

let
  home-manager = builtins.fetchTarball { 
    url = "https://github.com/nix-community/home-manager/archive/release-23.11.tar.gz"; 
  };
in
{
  imports =
    [
      ./hardware-configuration.nix
      "${home-manager}/nixos"
    ];
  
  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.kernelPackages = pkgs.linuxPackages_zen;

  networking.hostName = "hunter-pc-nixos"; # Define your hostname.

  # Enable networking
  networking.networkmanager.enable = true;

  # Set your time zone.
  time.timeZone = "America/Detroit";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";
  i18n.extraLocaleSettings = {
    LC_ADDRESS = "en_US.UTF-8";
    LC_IDENTIFICATION = "en_US.UTF-8";
    LC_MEASUREMENT = "en_US.UTF-8";
    LC_MONETARY = "en_US.UTF-8";
    LC_NAME = "en_US.UTF-8";
    LC_NUMERIC = "en_US.UTF-8";
    LC_PAPER = "en_US.UTF-8";
    LC_TELEPHONE = "en_US.UTF-8";
    LC_TIME = "en_US.UTF-8";
  };

  # Enable CUPS to print documents.
  services.printing.enable = true;

  # Enable sound with pipewire.
  sound.enable = true;
  hardware.pulseaudio.enable = false;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
  };

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.hunter = {
    isNormalUser = true;
    description = "Hunter LaFaille";
    extraGroups = [ "networkmanager" "wheel" "docker" "dialout"];
    packages = with pkgs; [
      firefox
      vscode
      spotify
      discord
      bitwarden
      htop
      traceroute
      nmap
      dbeaver
      flameshot
      winbox
      pkgs.home-manager # adds the home manager cli
    ];
  };

  # fonts
  fonts.fontconfig.enable = true;
  fonts.packages = with pkgs; [
    font-awesome
    noto-fonts
    noto-fonts-cjk
    noto-fonts-emoji
    liberation_ttf
    fira-code
    fira-code-symbols
    mplus-outline-fonts.githubRelease
    dina-font
    proggyfonts
  ];

  # home manager
  home-manager.users.hunter = {
    home.packages = with pkgs; [
      hyprpaper
      bemenu
    ];
    # alacritty (rust based gpu accelerated terminal)
    programs.alacritty = {
      enable = true;
      settings = {
        font = {
          normal = {
            family = "Fira Code";
            style = "Regular";
            size = 12.0;
          };
        };
      };
    };

    # waybar (top bar)
    programs.waybar = {
      enable = true;
    };
    
    # hyprpaper (wallpaper)
    home.file.".config/hypr/hyprpaper.conf".text = ''
    preload = /etc/nixos/wallpaper.png
    wallpaper = DP-1,/etc/nixos/wallpaper.png
    wallpaper = DP-2,/etc/nixos/wallpaper.png
    splash = false
    '';

    # hyprland (tiling wayland compositor / window manager)
    wayland.windowManager.hyprland = {
      enable = true;
      settings = {
        "$mod" = "SUPER";
        "exec-once" = "waybar & hyprpaper";
        "$menu" = "bemenu-run -b -i -p ''";
        general = {
          "col.active_border" = "rgb(a8a8a8)";
          "cursor_inactive_timeout" = 1; 
       };
        bind = [
          "$mod, D, exec, $menu"
          "$mod SHIFT, Q, killactive"
          "$mod SHIFT, Right, movewindow, r"
          "$mod SHIFT, Left, movewindow, l"
          "$mod, Right, movefocus, r"
          "$mod, Left, movefocus, l"
        ];
        misc = {
          "disable_hyprland_logo" = true;
        };
      };
      extraConfig = ''
      monitor=DP-1, 1920x1080@144, 1920x0, 1
      monitor=DP-2, 1920x1080@144, 0x0, 1
      '';
    };
    
    # should be the same as nixos version
    home.stateVersion = "23.11";
  };
  
  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    docker
    docker-compose
    git
    gcc
    killall
    polkit_gnome
  ];
  
  # enable docker
  virtualisation.docker.enable = true;

  # system wide shell configuration
  programs.fish.enable = true;
  users.defaultUserShell = pkgs.fish;
  
  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  # no touchie :)
  system.stateVersion = "23.11"; # Did you read the comment?

}
